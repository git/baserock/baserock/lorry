#!/bin/sh
#
# Tests incremental conversion of a simple hg repository to git.
#
# Copyright (C) 2012, 2020  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


set -e

repo="$DATADIR/hg-test-repo"

logfile="$DATADIR/hg-test-repo.log"
workdir="$DATADIR/work-dir"

"${SRCDIR}/test-lorry" --verbose --pull-only --log="$logfile" --working-area="$workdir" \
  "$DATADIR/hg-test-repo.lorry" > /dev/null

# make a second commit
(
  cd "$repo"
  echo "second line" >> test.txt
  hg commit --user lorry-test-suite --quiet -m "second commit"
)

"${SRCDIR}/test-lorry" --verbose --pull-only --log="$logfile" --working-area="$workdir" \
  "$DATADIR/hg-test-repo.lorry" > /dev/null

# verify that the git repository was created correctly
(
  cd "$workdir/hg-test-repo/git-b/"

  # list the branches
  git show-ref | cut -d' ' -f2

  # cat the test file
  git cat-file blob master:test.txt

  # list the commit messages
  git log --pretty='%s' master
)
