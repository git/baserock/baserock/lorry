#!/bin/sh
#
# Creates gzip/bzip2/lzma tarballs, each with a single file.
#
# Copyright (C) 2012, 2015  Codethink Limited
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


set -e

# create the original "repository"
repo="$DATADIR/tar-test-repo"
mkdir "$repo"
echo "first line" > "$repo/test.txt"

# create the tarballs
cd "$DATADIR"
tar -czf tar-test-repo.tar.gz "`basename $repo`"
tar -cjf tar-test-repo.tar.bz2 "`basename $repo`"
tar -cf tar-test-repo.tar "`basename $repo`"
xz -z tar-test-repo.tar

# create the .lorry file for the tarball "repositories"
cat <<EOF > $DATADIR/tar-test-repo.lorry
{
  "tar-test-repo-gzip": {
    "type": "tarball",
    "url": "file://$DATADIR/tar-test-repo.tar.gz"
  },
  "tar-test-repo-bzip2": {
    "type": "tarball",
    "url": "file://$DATADIR/tar-test-repo.tar.bz2"
  },
  "tar-test-repo-lzma": {
    "type": "tarball",
    "url": "file://$DATADIR/tar-test-repo.tar.xz"
  }
}
EOF

# create the working directory
test -d "$DATADIR/work-dir" || mkdir "$DATADIR/work-dir"
